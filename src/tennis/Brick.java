package tennis;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class Brick {
	
	private int width = 20, height = 10;
	private int xMin = 0, xMax, yMin = 50, yMax;
	private int brickPosX, brickPosY;
	Game game;
	
	public Brick(Game game, int frameWidth, int frameHeight) {
		super();
	
		this.game = game;
		
		xMax = frameWidth - width;
		yMax = frameHeight - frameHeight/2;
		
		//generate random pos x,y brick
		Random randomGenerator = new Random();
		brickPosX = randomGenerator.nextInt(xMax - xMin) + xMin;
		brickPosY = randomGenerator.nextInt(yMax - yMin) + yMin;
		
		System.out.println(brickPosX);
		System.out.println(brickPosY);
				
	}


	public void paint(Graphics g) {

		g.setColor(new Color(0, 128, 64));
		g.fillRect(brickPosX, brickPosY, width, height);
	}
	
	public Rectangle getBounds() {
		
		return new Rectangle(brickPosX, brickPosY, width, height);
		
	}
	
	public int getX() { 
		return brickPosX;
	}
	
	public int getY() { 
		return brickPosY;
	}
	
	public int getwidth() { 
		return width;
	}
	
	public int getheight() { 
		return height;
	}

}
