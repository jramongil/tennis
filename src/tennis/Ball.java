package tennis;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Ball {
	
	int x = 0, y = 0, xa = 1, ya = 1, diameter = 15, speed;
	Game game;
	
	public Ball(Game game) {
		super();
		this.game = game;
	}

	void move() {
		
		x += xa;
		y += ya;
		
		if (x < 0) { //borde izqdo
			Sound.BALL.play();
			xa = game.getSpeed(); 
		}

		if (y < 0) { //borde superior
			Sound.BALL.play();
			ya = game.getSpeed(); 
		}

		if (x > game.getWidth() - diameter) { //borde dcho
			Sound.BALL.play();
			xa = -game.getSpeed();
		}

		if (y > game.getHeight() - diameter) { //borde inferior
			Sound.BALL.play();
			game.gameOver();
		}
		
		if (collisionRacquet()) {
			Sound.BALL.play();
			ya = -game.getSpeed();
			y = game.racquet.getTopY() - diameter; //colocar pelota por encima de la raqueta
			//game.setSpeed(game.getSpeed() + 1);
		}
		
		if (collisionBrick()) {
			Sound.BALL.play();
			
			if (y >= game.brick.getY() + (game.brick.getheight() / 2)) { //ball hits brick top
				
				ya = -game.getSpeed();
				
			} 
			
			if (y <= game.brick.getY() - (game.brick.getheight() / 2)) { //ball hits brick bottom
				
				ya = game.getSpeed();
				
			}
			
			if (x < game.brick.getX()) { //ball hits brick left
				
				xa = -game.getSpeed();
				
			}

			if (x > game.brick.getX()) { //ball hits brick right
	
				xa = game.getSpeed();
	
			}
			
			
			
			//remove brick
			
		
		}

	}
	
	public void paint(Graphics g) {
	
		g.setColor(Color.GRAY);
		g.fillOval(x, y, diameter, diameter);
	}
	
	public Rectangle getBounds() {
		
		return new Rectangle(x, y, diameter, diameter);
		
	}
	
	private boolean collisionRacquet () {
		
		return game.racquet.getBounds().intersects(getBounds());
		
	}
	
	private boolean collisionBrick () {
		
		return game.brick.getBounds().intersects(getBounds());
		
	}

}
