package tennis;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class Racquet {
	
	int x = 0, xa = 0, y = 0;
	int width = 80, height = 15;
	Game game;
	
	public Racquet(Game game, int frameWidth, int frameHeight) {
		super();
	
		this.game = game;
		
		//centrar raqueta en pantalla al inicio de la partida
		x = frameWidth/2 - width/2;
		y = frameHeight - 100;
				
	}
	
	void move() { //definir l�mites movimiento raqueta

		if (x + xa < 0 ) //borde izq
			x = 0;
		else {
			if (x + xa > game.getWidth() - width) //borde dch
				x = game.getWidth() - width;
			else
				x += xa;
		}

	}
	
	public void mouseMoved(MouseEvent e) {
		
		x = e.getX();
		
	}
	
	
	public void keyReleased(KeyEvent e) {
				
		xa = 0;
		
	}
	
	public void keyPressed(KeyEvent e) {

		switch (e.getKeyCode()) {
		case KeyEvent.VK_RIGHT:
			xa = game.getSpeed();
			break;
		case KeyEvent.VK_LEFT:
			xa = -game.getSpeed();
			break;

		default:
			break;
		}


	}
	
	public void paint(Graphics g) {
		
		g.setColor(Color.WHITE);
		g.fillRoundRect(x, y, width, height, 25, 25);
	}
	
	public Rectangle getBounds() {
		
		return new Rectangle(x, y, width, height);
		
	}
	
	public int getTopY() { 
		return y - height;
	}
	

}
