package tennis;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Game extends JPanel{
	
	int x = 0, y = 0, xa = 1, ya = 1, speed = 1;
	static int speedBall = 5, frameWidth = 300, frameHeight = 500;
	Ball ball = new Ball(this);
	Racquet racquet = new Racquet(this, frameWidth, frameHeight);
	Brick brick = new Brick(this, frameWidth, frameHeight);
			
	public Game() {
		
		addMouseMotionListener(new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent e) {
				
				//Hide mouse pointer within frame
				Toolkit toolkit = Toolkit.getDefaultToolkit();
			    Point hotSpot = new Point(0,0);
			    BufferedImage cursorImage = new BufferedImage(1, 1, BufferedImage.TRANSLUCENT); 
			    Cursor invisibleCursor = toolkit.createCustomCursor(cursorImage, hotSpot, "InvisibleCursor");        
			    setCursor(invisibleCursor);
				
				racquet.mouseMoved(e);
			}
			
			@Override
			public void mouseDragged(MouseEvent arg0) {
				
				
			}
		});
			
		
		addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) {
								
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				racquet.keyReleased(e);
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				racquet.keyPressed(e);
				
			}
		});
		
		setFocusable(true); //Para que un objeto JPanel reciba las notificaciones del teclado
		Sound.BACK.loop();
		
	}

	public static void main(String[] args) {
		
		JFrame frame = new JFrame("Tennis");
		Game game = new Game();
		frame.add(game);
		frame.setSize(frameWidth, frameHeight);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//center frame on screen
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
						
		while (true) {
			game.move();
			game.repaint();
			try {
				Thread.sleep(speedBall);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}	
	
	@Override
	public void paintComponent(Graphics g) {
		// paints the background, borders etc.
		setBackground(new Color(0, 64, 128));
		
		super.paintComponent(g); //limpia la pantalla
		// all custom drawing AFTER this line..
		
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON); //redefine bordes objeto
				
		ball.paint(g2d);
		racquet.paint(g2d);
		brick.paint(g2d);
		
		//Score
		g2d.setColor(Color.GRAY);
		g2d.setFont(new Font("Verdana", Font.BOLD, 30));
		g2d.drawString(String.valueOf(getScore()), 10, 30);
	}
	
	void move() {
		ball.move();
		racquet.move();
	}
	
	public void gameOver() {
		
		Sound.BACK.stop();
		Sound.GAMEOVER.play();
		JOptionPane.showMessageDialog(this, "Tu puntuación es: " + getScore(), "Game Over", JOptionPane.YES_NO_OPTION);
		System.exit(ABORT);
		
	}
	
	private int getScore () {
		
		return speed - 1;
	}
	
	public int getSpeed () {
		
		return speed;
	}
	
	public void setSpeed (int speed) {
		
		this.speed = speed;
	}

}

